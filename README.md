# Network Speed Test Service

### Notes
- A web interface can be access throw: http://localhost:8080/
- A standard GET endpoint is available at http://localhost:8080/speed 

### Prerequisites

```
- Java 11 +
- Maven 3+
- Docker (Optional)

```


## Swagger EndPoint

http://localhost:8080/swagger-ui.html#

### Installing

It can be run as a Docker container or as a Java Spring Boot application:

Using Docker: 

```
- cd [PATH]
- docker build -t test-service:1.0 .
- docker run -p 127.0.0.1:8080:8080 --name test-service -d test-service:1.0 
```

Using Java and Maven:
By Command line:

```
- cd [PATH]
- mvn clean install
- Run SpringTestBoot file 
```

Importing the project IDE

```
- Open the project (I used Intellij for this test)
- Run SpringTestBoot.
```

## Running the tests

A simple Service Test using Spring boot test to validate the network speed. 

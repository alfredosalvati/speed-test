package com.test.controller;

import com.test.dto.Speed;
import com.test.service.SpeedService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SpeedController {

    public final SpeedService speedService;
    @Autowired
    SpeedController(SpeedService speedService){
        this.speedService = speedService;
    }


    @GetMapping(value="/speed", produces = MediaType.APPLICATION_JSON_VALUE)
    public Speed getSpeed() throws Exception {
        return speedService.getSpeed();
    }
}
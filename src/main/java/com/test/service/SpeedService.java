package com.test.service;

import com.test.dto.Speed;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.net.URL;

@Service
@Slf4j
public class SpeedService {

    @Value("${speedtest.url}")
    String url;

    static final int MBS = 10;
    static final double MBITS = MBS * 8;

    public Speed getSpeed() {
        log.debug("Test Started");

        double mbitsPerSec;
        long start = System.currentTimeMillis();
        this.downloadFile(url, "TestFile.txt");
        double elapsedTime = System.currentTimeMillis() - start;
        double secsToDownload = elapsedTime / 1000;
        mbitsPerSec = MBITS / secsToDownload;
        log.debug("mbitsPerSec: " + mbitsPerSec);
        return new Speed(Double.valueOf(mbitsPerSec).toString());
    }

    public static void downloadFile(String url, String fileName){
        try {
            FileUtils.copyURLToFile(
                    new URL(url),
                    new File(fileName),
                    100000,
                    10000);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}


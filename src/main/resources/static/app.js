function callEndpoint() {
    $.getJSON('/speed', function(speed) {
        showSpeed(speed.content);
        $("#loading").hide();
    });
}

function showSpeed(message) {
    $("#speed").append("<tr><td>" + message + "</td></tr>");
}

$(function () {
    $("form").on('submit', function (e) {
        e.preventDefault();
    });
    $( "#send" ).click(function() {
        $("#loading").show();
        callEndpoint();
    });
});
package com.test.service.controller;

import com.test.ServiceTestBoot;
import com.test.controller.SpeedController;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@ContextConfiguration(classes = ServiceTestBoot.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class SpeedControllerTest {

    private MockMvc mockMvc;
    private static final String SPEED_ENDPOINT = "/speed";
    @Autowired
    SpeedController speedController;

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders
                .standaloneSetup(speedController)
                .build();
    }

    @Test
    public void getSpeedSUCCESS() throws Exception {
        // when
        MvcResult result = mockMvc.perform(get(SPEED_ENDPOINT)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content", is(notNullValue())))
                .andReturn();

        assertThat(result, is(notNullValue()));
        assertEquals(result.getResponse().getStatus(), MockHttpServletResponse.SC_OK);
    }

}

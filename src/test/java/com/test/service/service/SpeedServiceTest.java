package com.test.service.service;

import com.test.ServiceTestBoot;
import com.test.dto.Speed;
import com.test.service.SpeedService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@ContextConfiguration(classes = ServiceTestBoot.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class SpeedServiceTest {

    @Autowired
    SpeedService speedService;

    @Test
    public void testGetSpeed(){
        Speed speed = speedService.getSpeed();
        assertThat(speed, is(notNullValue()));
        assertThat(speed.getContent(), is(notNullValue()));
    }

}
